import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PrivateLayoutComponent } from './components/private-layout/private-layout.component';
import { SavedHeroesComponent } from './components/saved-heroes/saved-heroes.component';
import { SearchHeroesComponent } from './components/search-heroes/search-heroes.component';


const routes: Routes = [
    {
        path: '',
        component: PrivateLayoutComponent ,
        children:[
            { path: 'saved-heroes', component: SavedHeroesComponent},
            { path: 'search-heroes', component: SearchHeroesComponent}
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PrivateRoutingModule { }
