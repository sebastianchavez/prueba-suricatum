import { Injectable, Output, EventEmitter } from '@angular/core';
import { HttpClient,  } from '@angular/common/http';

import { map } from 'rxjs/operators';
import { Heroe } from 'src/app/core/models/heroe';
import { GLOBAL } from 'src/app/configs/AppSettings';

@Injectable()
export class PrivateService {
  
  @Output() change: EventEmitter<boolean> = new EventEmitter();
  public url: String;
  constructor(public _http: HttpClient) {
    this.url = GLOBAL.url;
  }

  getHeroes() {
    return this._http.get(this.url + 'superheroes/', { responseType: 'text' })
    .pipe(
      map(response => {
        const html = document.createElement('html');
        html.innerHTML = response;
        const bd = html.getElementsByTagName('body');
        const bdStr = bd[0].firstChild.nodeValue;
        const data = JSON.parse(bdStr);
        return data;
      }));
  }

  
}