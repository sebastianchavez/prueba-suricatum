import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrivateLayoutComponent } from './components/private-layout/private-layout.component';
import { SavedHeroesComponent } from './components/saved-heroes/saved-heroes.component';
import { SearchHeroesComponent } from './components/search-heroes/search-heroes.component';
import { PrivateRoutingModule } from './private-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { NgxPaginationModule } from 'ngx-pagination'; 
import { FilterPipe } from 'src/app/core/pipes/filter.pipe';
import { FormsModule } from '@angular/forms';
import { LeftLayoutHeroesComponent } from './components/left-layout-heroes/left-layout-heroes.component';

@NgModule({
  declarations: [PrivateLayoutComponent, SavedHeroesComponent, SearchHeroesComponent,FilterPipe, LeftLayoutHeroesComponent
  ],
  imports: [
    CommonModule,
    PrivateRoutingModule,
    HttpClientModule,
    NgxPaginationModule,
    FormsModule
  ],
  exports:[
    PrivateLayoutComponent
  ]
})
export class PrivateModule { }
