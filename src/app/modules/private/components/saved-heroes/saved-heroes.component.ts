import { Component, OnInit, } from '@angular/core';
import { PrivateService } from '../../services/private.service';
import { Heroe } from 'src/app/core/models/heroe';
import { Router } from '@angular/router';

@Component({
  selector: 'app-saved-heroes',
  templateUrl: './saved-heroes.component.html',
  styleUrls: ['./saved-heroes.component.scss'],
  providers: [PrivateService]
})
export class SavedHeroesComponent implements OnInit {
  public heroes: Heroe[];
  public leftLayoutHeroes: Heroe[];

  constructor(private _router: Router) { }

  ngOnInit() {
    this.heroes = JSON.parse(localStorage.getItem('savedHeroes'));
    JSON.parse(localStorage.getItem('leftLayoutHeroes')) ? this.leftLayoutHeroes = JSON.parse(localStorage.getItem('leftLayoutHeroes')) : this.leftLayoutHeroes = [];
  }

  addHeroe(heroe: Heroe) {
    this.leftLayoutHeroes.push(heroe);
    localStorage.setItem('leftLayoutHeroes', JSON.stringify(this.leftLayoutHeroes));
    this.leftLayoutHeroes = JSON.parse(localStorage.getItem('leftLayoutHeroes'));
    this._router.navigate(['/private/saved-heroes']);
  }


  deleteHeroe(heroe: Heroe) {
    let index;
    JSON.parse(localStorage.getItem('leftLayoutHeroes')).forEach(function (temp, indexTemp) {
      if (heroe.name == temp.name) {
        index = indexTemp;
      }
    });
    this.leftLayoutHeroes.splice(index, 1);
    localStorage.setItem('leftLayoutHeroes', JSON.stringify(this.leftLayoutHeroes));
    this._router.navigate(['/private/saved-heroes']);   
  }
}


