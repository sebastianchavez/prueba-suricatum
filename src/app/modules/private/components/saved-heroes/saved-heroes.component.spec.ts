import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SavedHeroesComponent } from './saved-heroes.component';

describe('SavedHeroesComponent', () => {
  let component: SavedHeroesComponent;
  let fixture: ComponentFixture<SavedHeroesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SavedHeroesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SavedHeroesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
