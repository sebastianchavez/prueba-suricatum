import { Component, OnInit, SimpleChanges, OnChanges } from '@angular/core';
import { Heroe } from 'src/app/core/models/heroe';

@Component({
  selector: 'app-left-layout-heroes',
  templateUrl: './left-layout-heroes.component.html',
  styleUrls: ['./left-layout-heroes.component.scss']
})
export class LeftLayoutHeroesComponent implements OnInit, OnChanges {
  public paginaLeftLayout: number;
  public heroeModal: Heroe;
  public leftLayoutHerores: Heroe[];

  constructor() { }

  ngOnChanges(changes: SimpleChanges) {
    console.log(changes)
  }

  ngOnInit() {
    console.log(this.leftLayoutHerores)
    this.getHeroesLeftLayout()
  }

  getHeroesLeftLayout() {
    JSON.parse(localStorage.getItem('leftLayoutHeroes')) ? this.leftLayoutHerores = JSON.parse(localStorage.getItem('leftLayoutHeroes')) : this.leftLayoutHerores = [];
  }

  setHeroeModal(heroe) {
    this.heroeModal = heroe;
  }
}
