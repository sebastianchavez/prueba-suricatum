import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeftLayoutHeroesComponent } from './left-layout-heroes.component';

describe('LeftLayoutHeroesComponent', () => {
  let component: LeftLayoutHeroesComponent;
  let fixture: ComponentFixture<LeftLayoutHeroesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeftLayoutHeroesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeftLayoutHeroesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
