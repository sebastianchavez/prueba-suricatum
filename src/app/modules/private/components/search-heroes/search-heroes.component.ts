import { Component, OnInit } from '@angular/core';
import { PrivateService } from '../../services/private.service';
import { Heroe } from 'src/app/core/models/heroe';

@Component({
  selector: 'app-search-heroes',
  templateUrl: './search-heroes.component.html',
  styleUrls: ['./search-heroes.component.scss'],
  providers: [PrivateService]
})
export class SearchHeroesComponent implements OnInit {

  public searcHeroes: Heroe[] = [];
  public pagina: number;
  public savedHeroes: Heroe[] = [];
  filterPost = '';

  constructor(private _privateService: PrivateService) { }


  ngOnInit() {
    JSON.parse(localStorage.getItem('savedHeroes')) ? this.savedHeroes = JSON.parse(localStorage.getItem('savedHeroes')) : this.savedHeroes = [];
    this.getHerores(this.pagina);
    this.pagina = 1;
  }

  getHerores(pagina: number) {
    this._privateService.getHeroes().subscribe(
      response => {
        this.searcHeroes = response;
        console.log(this.searcHeroes)
      },
      error => {
        console.error(error);
      }
    )
  }

  saveHeroe(heroe: Heroe) {
    this.savedHeroes.push(heroe);
    localStorage.setItem('savedHeroes', JSON.stringify(this.savedHeroes));
  }


}
