import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SsoLayoutComponent } from './components/sso-layout/sso-layout.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { SsoRoutingModule } from './sso-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
@NgModule({
  declarations: [SsoLayoutComponent, LoginComponent, RegisterComponent],
  imports: [
    CommonModule,
    SsoRoutingModule,
    ReactiveFormsModule
  ],
  exports:[
    SsoLayoutComponent
  ]
})
export class SsoModule { }
