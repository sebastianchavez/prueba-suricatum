import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { User } from 'src/app/core/models/user';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {


  public registeredUsers : Array<User> = [];
  public status : boolean;
  registerForm= new  FormGroup({
    user: new FormControl(''),
    password: new FormControl('')
  })

  constructor() { }

  ngOnInit() {
  }

  onSubmit(){
    let user : User = new User();
    user.user = this.registerForm.get('user').value;
    user.password = this.registerForm.get('password').value
    this.registeredUsers.push(user);
    localStorage.setItem('usuarios', JSON.stringify(this.registeredUsers));
    this.status=true;

  }

}
