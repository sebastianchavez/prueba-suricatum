import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { User } from 'src/app/core/models/user';
import { Router } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public status : boolean;
  public registeredUsers: Array<User> = [];

  loginForm = new FormGroup({
    user: new FormControl(''),
    password: new FormControl('')
  })

  constructor(private _router: Router) { }

  ngOnInit() {
  }

  onSubmit() {
    let user: User = new User();

    user.user = this.loginForm.get('user').value;
    user.password = this.loginForm.get('password').value
    this.registeredUsers = JSON.parse(localStorage.getItem('usuarios'));
    this.registeredUsers.forEach(userTmp => {
      if (userTmp.user == user.user && userTmp.password == user.password) {
        this._router.navigate(['/private/search-heroes']);

      } else {
        this.status = true;
      }
    }

    );

  }

}
