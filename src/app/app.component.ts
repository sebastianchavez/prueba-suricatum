import { Component } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'prueba';

  constructor(private _router: Router){

  }
  ngOnInit(){
    this._router.routeReuseStrategy.shouldReuseRoute = function(){ 
      return false; 
  }; 
  
  this._router.events.subscribe((evt) => { 
      if (evt instanceof NavigationEnd) { 
       this._router.navigated = false; 
       window.scrollTo(0, 0); 
      } 
  }); 
  }
}
