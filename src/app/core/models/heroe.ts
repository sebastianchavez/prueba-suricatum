export class Heroe{
    name: string;
    picture: string;
    publisher: string;
    info: string;
}