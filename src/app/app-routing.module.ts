import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SsoLayoutComponent } from './modules/sso/components/sso-layout/sso-layout.component';
const routes: Routes = [
  {
    path: '', loadChildren: './modules/sso/sso.module#SsoModule'
  },
  {
    path: 'private', loadChildren: './modules/private/private.module#PrivateModule'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
